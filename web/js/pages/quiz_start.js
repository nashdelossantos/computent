$(function() {
	var listGroup = $('.list-group-item');
	listGroup.hover(function(e) {
		var action = $(this).find('a.resume');

		$('a.resume').addClass('hidden');
		action.removeClass('hidden');
	});

	// Quiz starts
	var questionDiv 	= $('#question-div'),
		qitem 			= $('.qitem'),
		totalQ 			= qitem.length,
		currentQid 		= qitem.first().data('id'),
		currentQ 		= qitem.first(),
		$previous 		= $('#previous'),
		$next 			= $('#next'),
		$finish 		= $('#finish'),
		$directionBtn 	= $('.directionBtn'),
		answerPath 		= $('input[name=answerPath]').val()
	;

	qitem.first().removeClass('hidden');

	// << Next or Previous
	$directionBtn.click(function(e) {
		e.preventDefault();

		var sel   			= currentQ.find('input:checked'),
			selQuizid 		= sel.data('quizid'),
			selQuestionid 	= sel.data('questionid'),
			selOptionid 	= sel.data('optionid')
		;

		// save selection
		$.ajax({
			url 		: answerPath,
			type 		: 'post',
			dataType 	: 'json',
			data 		: {quizid: selQuizid, questionid: selQuestionid, optionid: selOptionid},
			success 	: function(data) {
				console.log(data);
			},
			error 		: function(data) {
				console.log(data);
			}
		});

		hideNshowQ($(this).data('direction'));

	});
	// >> Next or Previous

	// << Hide Question
	function hideNshowQ(direction)
	{
		currentQ.addClass('animated fadeOut');
		setTimeout(function(e) {
			currentQ.addClass('hidden').removeClass('animated fadeOut');

			if (direction == 'forward') {
				currentQid++;
			} else {
				currentQid--;
			}

			currentQ = $('.qitem[data-id='+currentQid+']');

			$('.qitem[data-id='+currentQid+']').removeClass('hidden').addClass('animated fadeIn');

			if (currentQid > 1) {
				if ($previous.hasClass('hidden')) {
					$previous.removeClass('hidden');
				}
			} else {
				if (!$previous.hasClass('hidden')) {
					$previous.addClass('hidden');
				}
			}
			if (currentQid < totalQ) {
				if ($next.hasClass('hidden')) {
					$next.removeClass('hidden');
				}
			} else {
				$next.addClass('hidden');
			}

			if (currentQid == totalQ) {
				if ($finish.hasClass('hidden')) {
					$finish.removeClass('hidden');
				}
			} else {
				if (!$finish.hasClass('hidden')) {
					$finish.addClass('hidden');
				}
			}

		},1);

		return;
	}
	// >> Hide Question
});