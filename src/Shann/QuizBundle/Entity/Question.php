<?php

namespace Shann\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Shann\QuizBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Topic")
     * @ORM\JoinColumn(name="topic", referencedColumnName="id")
     */
    private $topic;

    /**
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity="Option", mappedBy="question")
     */
    private $options;

    /**
     * @ORM\Column(name="is_multiple", type="boolean", nullable=true)
     */
    private $isMultiple = null;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add option
     *
     * @param \Shann\QuizBundle\Entity\Option $option
     *
     * @return Question
     */
    public function addOption(\Shann\QuizBundle\Entity\Option $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \Shann\QuizBundle\Entity\Option $option
     */
    public function removeOption(\Shann\QuizBundle\Entity\Option $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set topic
     *
     * @param \Shann\QuizBundle\Entity\Topic $topic
     *
     * @return Question
     */
    public function setTopic(\Shann\QuizBundle\Entity\Topic $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \Shann\QuizBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set isMultiple
     *
     * @param boolean $isMultiple
     *
     * @return Question
     */
    public function setIsMultiple($isMultiple)
    {
        $this->isMultiple = $isMultiple;

        return $this;
    }

    /**
     * Get isMultiple
     *
     * @return boolean
     */
    public function getIsMultiple()
    {
        return $this->isMultiple;
    }
}
