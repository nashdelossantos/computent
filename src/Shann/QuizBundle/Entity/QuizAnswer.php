<?php

namespace Shann\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuizAnswer
 *
 * @ORM\Table(name="quiz_answer")
 * @ORM\Entity(repositoryClass="Shann\QuizBundle\Repository\QuizAnswerRepository")
 */
class QuizAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Quiz")
     * @ORM\JoinColumn(name="quiz", referencedColumnName="id")
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question", referencedColumnName="id")
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="Option")
     * @ORM\JoinColumn(name="answer", referencedColumnName="id")
     */
    private $answer;

    /**
     * @ORM\Column(name="correct", type="boolean")
     */
    private $correct = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param \Shann\QuizBundle\Entity\Question $question
     *
     * @return QuizAnswer
     */
    public function setQuestion(\Shann\QuizBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Shann\QuizBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param \Shann\QuizBundle\Entity\Option $answer
     *
     * @return QuizAnswer
     */
    public function setAnswer(\Shann\QuizBundle\Entity\Option $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return \Shann\QuizBundle\Entity\Option
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set quiz
     *
     * @param \Shann\QuizBundle\Entity\Quiz $quiz
     *
     * @return QuizAnswer
     */
    public function setQuiz(\Shann\QuizBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \Shann\QuizBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Set correct
     *
     * @param boolean $correct
     *
     * @return QuizAnswer
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;

        return $this;
    }

    /**
     * Get correct
     *
     * @return boolean
     */
    public function getCorrect()
    {
        return $this->correct;
    }
}
