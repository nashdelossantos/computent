<?php

namespace Shann\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz")
 * @ORM\Entity(repositoryClass="Shann\QuizBundle\Repository\QuizRepository")
 */
class Quiz
{
    const QUIZZES_NUM_PER_SET = 10;
    const PASSING_PERCENTAGE  = 70;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Topic")
     * @ORM\JoinColumn(name="topic", referencedColumnName="id")
     */
    private $topic;

    /**
     * @ORM\OneToMany(targetEntity="QuizAnswer", mappedBy="quiz")
     */
    private $answers;

    /**
     * @ORM\Column(name="started", type="datetime", nullable=true)
     */
    private $started;

    /**
     * @ORM\Column(name="completed", type="datetime", nullable=true)
     */
    private $completed;

    /**
     * @ORM\Column(name="total_correct", type="integer", nullable=true)
     */
    private $totalCorrect;

    /**
     * @ORM\Column(name="total_wrong", type="integer", nullable=true)
     */
    private $totalWrong;

    /**
     * @ORM\Column(name="percentage", type="string", length=20, nullable=true)
     */
    private $percentage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set started
     *
     * @param \DateTime $started
     *
     * @return Quiz
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
    * Get completed
    * @return
    */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
    * Set completed
    * @return $this
    */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
        return $this;
    }

    /**
     * Set totalCorrect
     *
     * @param integer $totalCorrect
     *
     * @return Quiz
     */
    public function setTotalCorrect($totalCorrect)
    {
        $this->totalCorrect = $totalCorrect;

        return $this;
    }

    /**
     * Get totalCorrect
     *
     * @return integer
     */
    public function getTotalCorrect()
    {
        return $this->totalCorrect;
    }

    /**
     * Set totalWrong
     *
     * @param integer $totalWrong
     *
     * @return Quiz
     */
    public function setTotalWrong($totalWrong)
    {
        $this->totalWrong = $totalWrong;

        return $this;
    }

    /**
     * Get totalWrong
     *
     * @return integer
     */
    public function getTotalWrong()
    {
        return $this->totalWrong;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     *
     * @return Quiz
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set topic
     *
     * @param \Shann\QuizBundle\Entity\Topic $topic
     *
     * @return Quiz
     */
    public function setTopic(\Shann\QuizBundle\Entity\Topic $topic = null)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \Shann\QuizBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \Shann\QuizBundle\Entity\QuizAnswer $answer
     *
     * @return Quiz
     */
    public function addAnswer(\Shann\QuizBundle\Entity\QuizAnswer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \Shann\QuizBundle\Entity\QuizAnswer $answer
     */
    public function removeAnswer(\Shann\QuizBundle\Entity\QuizAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
