<?php

namespace Shann\QuizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Shann\QuizBundle\Entity\Topic;
use Shann\QuizBundle\Form\TopicType;
use Shann\QuizBundle\Entity\Quiz;
use Shann\QuizBundle\Entity\Question;
use Shann\QuizBundle\Entity\Option;

class TopicController extends Controller
{
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$topics = $em->getRepository('ShannQuizBundle:Topic')->findAll();

		return $this->render('ShannQuizBundle:Topic:index.html.twig', array(
			'topics' 	=> $topics,
		));
	}

	/**
     * View Topic Details
     *
     * @param  integer $id [description]
     * @return [type]      [description]
     */
    public function detailAction($id = 0)
    {
    	$em = $this->getDoctrine()->getManager();

    	$topic = $em->getRepository('ShannQuizBundle:Topic')->findOneById($id);

        return $this->render('ShannQuizBundle:Topic:detail.html.twig',
        	array(
        		'topic' => $topic,
        	)
        );
    }

    /**
     * New Topic
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $topic = new Topic();

        $form = $this->createForm(TopicType::class, $topic);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $topic = $form->getData();
            $em->persist($topic);
            $em->flush();

            $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Topic has been successfully created!')
            ;

            return $this->redirectToRoute('topic_question_add', array('id' => $topic->getId()));
        }

        return $this->render('ShannQuizBundle:Topic:form.html.twig',
            array(
                'form'  => $form->createView(),
            )
        );
    }

    /**
     * Add Questions to Topic
     *
     * @param integer $id [description]
     */
    public function addQuestionAction(Request $request, $id = 0)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$topic = $em->getRepository('ShannQuizBundle:Topic')->findOneById($id)) {
            throw new NotFoundHttpException("Topic Not Found!");
        }

        if ($request->isMethod('POST')) {
            $data       = $request->request;

            $topicId    = $data->get('topic_id');
            $questionTxt= $data->get('question');

            $choices    = $data->get('choices')['choice'];
            $answers    = $data->get('choices')['answer'];

            $question   = new Question();
            $question->setTopic($topic)
                     ->setText($questionTxt)
            ;

            $em->persist($question);

            for ($i=1; $i <= count($choices); $i++) {

                $option = new Option();
                $option->setQuestion($question)
                       ->setText($choices[$i])
                ;

                if (isset($answers[$i])) {
                    $option->setIsCorrect(true);
                }

                $em->persist($option);
                $question->addOption($option);
            }

            if (count($answers) > 1) {
                $question->setIsMultiple(true);
            }

            $em->flush();

            $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Question successfully added!')
            ;
        }

        return $this->render('ShannQuizBundle:Topic:form_question.html.twig', array(
            'topic' => $topic,
        ));
    }
}
