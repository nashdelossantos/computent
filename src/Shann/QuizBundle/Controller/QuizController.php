<?php

namespace Shann\QuizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

use Shann\QuizBundle\Entity\Question;
use Shann\QuizBundle\Entity\Option;
use Shann\QuizBundle\Entity\Topic;
use Shann\QuizBundle\Entity\Quiz;
use Shann\QuizBundle\Entity\QuizAnswer;

class QuizController extends Controller
{
    public function indexAction()
    {
        return $this->render('ShannQuizBundle:Default:index.html.twig');
    }

    /**
     * Import Questions from CSV file
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function importAction(Request $request)
    {
    	$topic = null;

    	$form = $this->createFormBuilder(array())
    		->add('topic', TextType::class, array(
    			'attr'	=> array(
    				'class'	=> 'form-control'
    			)
    		))
    		->add('csvImport', FileType::class)
    		->getForm();

    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		if ($attachment = $request->files->get('form')) {
	    		$em 		= $this->getDoctrine()->getManager();

	    		$topic = $form->getData()['topic'];
    			$file = $attachment['csvImport'];

	    		$newTopic 	= new Topic();
	    		$newTopic->setName($topic);
	    		$em->persist($newTopic);

    			if ($file->getClientOriginalExtension() !== 'csv') {
    				throw new Exception("File type not allowed.", 1);
    			}

    			$contents = array_map('str_getcsv', file($file));
    			foreach ($contents as $key => $text) {
    				if ($key > 0) {
    					foreach ($text as $kt => $item) {
    						if ($kt == 0) {
		    					$question = new Question();
		    					$question
		    						->setTopic($newTopic)
		    						->setText($item)
		    					;
		    					$em->persist($question);
		    					$newTopic->addQuestion($question);
    						}
    						if ($kt > 0 && $kt < 5) {
		    					$option{$kt} = new Option();
		    					$option{$kt}
		    						->setText($item)
		    						->setQuestion($question)
		    					;
		    					$em->persist($option{$kt});
		    					$question->addOption($option{$kt});
    						}
    						if ($kt == 5) {
    							if (is_array($item)) {
    								foreach ($item as $answer) {
    									$option{$answer}->setIsCorrect(true);
    									$question->setIsMultiple(true);
    								}
    							} else {
    								$option{$item}->setIsCorrect(true);
    							}
    						}
    					}
    				}
    			}
    			$em->flush();

    			$request
    				->getSession()
    				->getFlashBag()
    				->add('success', 'The following set of questions and answers have been successfully imported to the system!')
    			;
    		}
    	}

    	return $this->render('ShannQuizBundle:Question:import.html.twig', array(
    		'form'	=> $form->createView(),
    		'topic'	=> isset($newTopic) ? $newTopic : $topic,
    	));
    }

    /**
     * Start Quiz
     *
     * @param  Request $request [description]
     * @param  integer $id      [description]
     * @return [type]           [description]
     */
    public function startAction(Request $request, $id = 0)
    {
        $em = $this->getDoctrine()->getManager();

        $topic      = $em->getRepository('ShannQuizBundle:Topic')->findOneById($id);

        if ($request->query->get('force')) {
            if ($quizzes = $em->getRepository('ShannQuizBundle:Quiz')->findBy(array('completed' => null))) {
                return $this->render('ShannQuizBundle:Quiz:start.html.twig', array(
                    'topic'     => $topic,
                ));
            }
        }

        $questions  = $em->getRepository('ShannQuizBundle:Question')->findForQuiz($topic);

        $quiz = new Quiz();
        $quiz
            ->setStarted(new \DateTime())
            ->setTopic($topic)
        ;

        foreach ($questions as $question) {
            $answer = new QuizAnswer();
            $answer
                ->setQuiz($quiz)
                ->setQuestion($question)
            ;
            $em->persist($answer);

            $quiz->addAnswer($answer);
        }

        $em->persist($quiz);
        $em->flush();

        return $this->render('ShannQuizBundle:Quiz:start.html.twig', array(
            'quiz'      => $quiz,
            'topic'     => $topic,
            'questions' => $questions,
        ));
    }

    /**
     * Resume a quiz
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function resumeAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$quiz = $em->getRepository('ShannQuizBundle:Quiz')->findOneById($id)) {
            throw new Exception("Quiz Not Found!", 1);
        }

        return $this->render('ShannQuizBundle:Quiz:start.html.twig', array(
            'quiz'      => $quiz,
            'questions' => $quiz->getAnswers(),
            'topic'     => $quiz->getTopic(),
        ));
    }

    /**
     * Finish Topic
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function finishAction(Request $request, $id = 0)
    {
        if (!$request->isMethod('POST')) {
            throw new Exception("Invalid Method", 1);
        }

        $em = $this->getDoctrine()->getManager();

        if (!$quiz = $em->getRepository('ShannQuizBundle:Quiz')->findOneById($id)) {
            throw new Exception("Quiz Not Found!", 1);
        }

        if (!$usersAnswers = $em->getRepository('ShannQuizBundle:QuizAnswer')->findByQuiz($quiz)) {
            throw new Exception("Quiz Not Found!", 1);
        }

        $correct = 0;
        $wrong   = 0;
        foreach ($usersAnswers as $answer) {
            if ($ans = $answer->getAnswer()) {
                if ($answer->getAnswer()->getIsCorrect()) {
                    $correct++;
                    $answer->setCorrect(true);
                    $em->persist($answer);
                } else {
                    $wrong++;
                }
            } else {
                $wrong++;
            }
        }

        $percent =  $correct / QUIZ::QUIZZES_NUM_PER_SET;
        $quiz
            ->setTotalCorrect($correct)
            ->setTotalWrong($wrong)
            ->setPercentage(number_format($percent * 100, 0))
            ->setCompleted(new \DateTime());

        $em->persist($quiz);
        $em->flush();

        return $this->redirectToRoute('quiz_result', array('id' => $quiz->getId()));
    }

    /**
     * Quiz Result
     *
     * @param  integer $id [description]
     * @return [type]      [description]
     */
    public function resultAction($id = 0)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$quiz = $em->getRepository('ShannQuizBundle:Quiz')->findOneById($id)) {
            throw new Exception("Quiz Not Found!", 1);
        }

        return $this->render('ShannQuizBundle:Quiz:result.html.twig', array(
            'quiz'  => $quiz,
        ));
    }

    /**
     * Ajax function to save option selection
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function saveAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('error' => true));
        }

        $quizId     = $request->request->get('quizid');
        $questionId = $request->request->get('questionid');
        $optionId   = $request->request->get('optionid');

        if ($quizId && $questionId && $optionId) {
            $em = $this->getDoctrine()->getManager();

            $quiz       = $em->getRepository('ShannQuizBundle:Quiz')->findOneById($quizId);
            $question   = $em->getRepository('ShannQuizBundle:Question')->findOneById($questionId);
            $option     = $em->getRepository('ShannQuizBundle:Option')->findOneById($optionId);

            if (!$questionAnswer = $em->getRepository('ShannQuizBundle:QuizAnswer')->findByQuizAndQuestion($quiz, $question)) {
                return new JsonResponse(array('error' => true));
            }

            $questionAnswer->setAnswer($option);

            $em->persist($questionAnswer);
            $em->flush();
        }

        return new JsonResponse(array('success' => true));
    }

    /**
     * Get quiz history
     *
     * @return [type] [description]
     */
    public function historyAction()
    {
        $em = $this->getDoctrine()->getManager();
        $quizzes = $em->getRepository('ShannQuizBundle:Quiz')->findAll();
dump($quizzes);
die();
        return $this->render('ShannQuizBundle:Quiz:history.html.twig', array(
            'quizzes'   => $quizzes,
        ));
    }
}
